package main

type HonestReferee struct {
	Referee
}

func (HonestReferee) Decide(event Event) Decision {
	return Decision{infraction: "Penalty", interpretation: "Rules Violation"}
}
