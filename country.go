package main

type Country string

const (
  BRASIL Country = "Brasil"
  ALEMANHA = "Alemanha"
  USA = "USA"
  ISRAEL = "Israel"
)
